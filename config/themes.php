<?php

return [
    'default' => 'default',

    'themes' => [
        'default' => [
            'views_path' => 'resources/themes/default/views',
            'assets_path' => 'public/themes/mint/assets',
            'name' => 'Default'
        ],
        'mint' => [
            'views_path' => 'resources/themes/mint/views',
            'assets_path' => 'public/themes/mint/assets',
            'name' => 'mint',
           // 'parent' => 'default'
        ]
        // 'bliss' => [
        //     'views_path' => 'resources/themes/bliss/views',
        //     'assets_path' => 'public/themes/bliss/assets',
        //     'name' => 'Bliss',
        //     'parent' => 'default'
        // ]
    ]
];