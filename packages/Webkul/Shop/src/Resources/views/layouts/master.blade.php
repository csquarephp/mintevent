<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>

   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <title>@yield('page_title')</title>
   <meta name="description" content="">
   <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, minimum-scale=1.0">
   <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="content-language" content="{{ app()->getLocale() }}">

    <link rel="stylesheet" href="{{ bagisto_asset('css/shop.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/webkul/ui/assets/css/ui.css') }}">

   <!-- CSS here -->
   <!-- all css here -->

   <link rel="stylesheet" href="{{ bagisto_asset('themes/mint/assets/css/mintplugins.css') }}">
   <link rel="stylesheet" href="{{ bagisto_asset('themes/mint/assets/css/mastermint.css') }}">

   <script src="assets/js/vendor/modernizr-2.8.3.min.js"></script>
   <link href="{{ bagisto_asset('themes/mint/assets/images/mlogo.png') }}" rel="apple-touch-icon-precomposed" sizes="48x48">
   <link href="{{ bagisto_asset('themes/mint/assets/images/mlogo.png') }}" rel="apple-touch-icon-precomposed">
   <link href="{{ bagisto_asset('themes/mint/assets/images/mlogo.png') }}" rel="shortcut icon">
   <link rel="apple-touch-icon" sizes="57x57" href="{{ bagisto_asset('themes/mint/assets/images/apple-icon-57x57.png') }}">
   <link rel="apple-touch-icon" sizes="60x60" href="{{ bagisto_asset('themes/mint/assets/images/apple-icon-60x60.png') }}">
   <link rel="apple-touch-icon" sizes="72x72" href="{{ bagisto_asset('themes/mint/assets/images/apple-icon-72x72.png') }}">
   <link rel="apple-touch-icon" sizes="76x76" href="{{ bagisto_asset('themes/mint/assets/images/apple-icon-76x76.png') }}">
   <link rel="apple-touch-icon" sizes="114x114" href="{{ bagisto_asset('themes/mint/assets/images/apple-icon-114x114.png') }}">
   <link rel="apple-touch-icon" sizes="120x120" href="{{ bagisto_asset('themes/mint/assets/images/apple-icon-120x120.png') }}">
   <link rel="apple-touch-icon" sizes="144x144" href="{{ bagisto_asset('themes/mint/assets/images/apple-icon-144x144.png') }}">
   <link rel="apple-touch-icon" sizes="152x152" href="{{ bagisto_asset('themes/mint/assets/images/apple-icon-152x152.png') }}">
   <link rel="apple-touch-icon" sizes="180x180" href="{{ bagisto_asset('themes/mint/assets/images/apple-icon-180x180.png') }}">
   <link rel="icon" type="image/png" sizes="192x192" href="{{ bagisto_asset('themes/mint/assets/images/android-icon-192x192.png') }}">
   <link rel="icon" type="image/png" sizes="32x32" href="{{ bagisto_asset('themes/mint/assets/images/favicon-32x32.png') }}">
   <link rel="icon" type="image/png" sizes="96x96" href="{{ bagisto_asset('themes/mint/assets/images/favicon-96x96.png') }}">
   <link rel="icon" type="image/png" sizes="16x16" href="{{ bagisto_asset('themes/mint/assets/images/favicon-16x16.png') }}">
   <link rel="manifest" href="{{ bagisto_asset('themes/mint/assets/images/manifest.json') }}">
   <meta name="msapplication-TileColor" content="#ffffff">
   <meta name="msapplication-TileImage" content="assets/images/ms-icon-144x144.png">

   <meta name="apple-mobile-web-app-capable" content="yes" />
   <meta name="theme-color" content="#ffffff" />
   <meta name="msapplication-navbutton-color" content="#ffffff">
   <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
   @if ($favicon = core()->getCurrentChannel()->favicon_url)
        <link rel="icon" sizes="16x16" href="{{ $favicon }}" />
    @else
        <link rel="icon" sizes="16x16" href="{{ bagisto_asset('themes/mint/assets/images/favicon.ico') }}" />
    @endif

    @yield('head')

    @section('seo')
        @if (! request()->is('/'))
            <meta name="description" content="{{ core()->getCurrentChannel()->description }}"/>
        @endif
    @show

    @stack('css')

    {!! view_render_event('bagisto.shop.layout.head') !!}
</head>

<body data-sticky_parent @if (core()->getCurrentLocale()->direction == 'rtl') class="rtl" @endif style="scroll-behavior: smooth;">
   <div class="overlay"></div>
   <header class="header-area">
      <div class="BXtpHeaderTp1 DeskTopOption">
         <div class="BxTp1Cnt1">
            <div class="BxTp1Left fullHeight align-items-center">
               <div class="sidemenu"><a href="#"><img class="TopBarIconTp1 mrgRight50" src="{{ bagisto_asset('themes/mint/assets/images/menu-icon.png') }}" alt="Side Menu" /></a>
                  <div id="sideNavigation" class="sidenav">
                     <div class="sidenav-inner">
                        <span class="MenuRightShape">&nbsp;</span>
                        <a href="javascript::" class="closebtn">&times;</a> <a href="about-us.php">
                           <h2>About us</h2>
                        </a>
                        <a href="download-catalog.php" class="fancybox fancybox.iframe dl-btn">
                           <h2>Dowloads</h2>
                        </a>
                        <a href="cart.php">
                           <h2>Cart</h2>
                        </a>
                        <ul>
                           <li><a href="faq.php">FAQs</a></li>
                           <li><a href="#">press release</a></li>
                           <li><a href="#">blog</a></li>
                           <li><a href="terms-and-conditions.php">terms & Conditions</a></li>
                           <li><a href="contact-us.php">Contact Us</a></li>
                           <li><a href="#">testimonials</a></li>
                        </ul>
                        <div class="clear"></div>
                        <div class="social-area02">

                           <a href="#" target="_blank"><i class="fa fb fa-facebook"></i></a>
                           <a href="#" target="_blank"><i class="fa twt fa-twitter"></i></a>
                           <a href="#" target="_blank"><i class="fa ig fa-instagram" aria-hidden="true"></i>
                           </a>
                        </div>
                     </div>
                  </div>

               </div>
               <div class="search-area searchlinkClickHndler">
                  <img src="{{ bagisto_asset('themes/mint/assets/images/search-icon.png') }}" alt="" class="TopBarIconTp1 mrgRight50" />

               </div>
               <div class="searchform">
                  <form action="" class="row m-0">
                     <div class="SearchFormBox col">
                        <div class="row">
                           <div class="col-auto">
                              <span class="searchlinkClickHndler d-flex  align-items-center fullHeight">
                                 <span class="CloseIcnTp1">x</span>
                                 <span class="closeIconTextTp1">Close</span>
                              </span>
                           </div>
                           <div class="col">
                              <input type="text" class="SearchFormField" placeholder="Search our range of thousands of products..">
                           </div>
                           <div class="col-auto">
                              <button type="button" class="SearchBtn1">Search</button>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
               <div class="LogoBoxTp1 DeskTopOption">
                  <div class="posRelative">
                     <a href="index.php" class="HeroLogo">
                        <img src="{{ bagisto_asset('themes/mint/assets/images/logo.png') }}" alt="Logo" />
                     </a>
                  </div>
               </div>
               <div class="d-flex  align-items-center fullHeight DeskTopOption">
                  <div class="NavLinkBox d-flex  align-items-center fullHeight">
                     <div class="NvLinkTp1 d-flex  align-items-center fullHeight">
                        <a href="products.php" class=" d-flex  align-items-center fullHeight justify-content-sm-center NvLinkTp1Item">Catalogue</a>
                        <div class="SubNavBox">
                           <div class="SubSystemBox">
                              <div class="SubItemTp1Box">
                                 <a href="products.php" class="SubNvLinkBoxLnk">New Arrivals</a>
                                 <div class="SubItemBoxTp1">
                                    <div class="flex-wrap SubItemBoxTp1Items d-flex  align-items-center fullHeight justify-content-sm-center">
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dreamscape</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Exotic Nomad</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Imperial</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Mexicola</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dark & Moody</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Retro Remix</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink"> French Coast</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">White Bliss</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Luxe Style</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="SubSystemBox">
                              <div class="SubItemTp1Box">
                                 <a href="products.php" class="SubNvLinkBoxLnk">Seating</a>
                                 <div class="SubItemBoxTp1">
                                    <div class="flex-wrap SubItemBoxTp1Items d-flex  align-items-center fullHeight justify-content-sm-center">
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dreamscape</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Exotic Nomad</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Imperial</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Mexicola</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dark & Moody</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Retro Remix</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink"> French Coast</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">White Bliss</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Luxe Style</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="SubSystemBox">
                              <div class="SubItemTp1Box">
                                 <a href="products.php" class="SubNvLinkBoxLnk">Tables</a>

                              </div>
                           </div>
                           <div class="SubSystemBox">
                              <div class="SubItemTp1Box">
                                 <a href="products.php" class="SubNvLinkBoxLnk">Bars & Food Stations</a>
                                 <div class="SubItemBoxTp1">
                                    <div class="flex-wrap SubItemBoxTp1Items d-flex  align-items-center fullHeight justify-content-sm-center">
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dreamscape</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Exotic Nomad</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Imperial</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Mexicola</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dark & Moody</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Retro Remix</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink"> French Coast</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">White Bliss</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Luxe Style</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="SubSystemBox">
                              <div class="SubItemTp1Box">
                                 <a href="products.php" class="SubNvLinkBoxLnk">Decor</a>
                                 <div class="SubItemBoxTp1">
                                    <div class="flex-wrap SubItemBoxTp1Items d-flex  align-items-center fullHeight justify-content-sm-center">
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dreamscape</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Exotic Nomad</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Imperial</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Mexicola</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dark & Moody</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Retro Remix</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink"> French Coast</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">White Bliss</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Luxe Style</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="SubSystemBox">
                              <div class="SubItemTp1Box">
                                 <a href="products.php" class="SubNvLinkBoxLnk">Decor</a>
                                 <div class="SubItemBoxTp1">
                                    <div class="flex-wrap SubItemBoxTp1Items d-flex  align-items-center fullHeight justify-content-sm-center">
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dreamscape</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Exotic Nomad</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Imperial</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Mexicola</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dark & Moody</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Retro Remix</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink"> French Coast</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">White Bliss</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Luxe Style</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="NvLinkTp1 d-flex  align-items-center fullHeight">
                        <a href="products.php" class=" d-flex  align-items-center fullHeight justify-content-sm-center NvLinkTp1Item">Collections</a>
                        <div class="SubNavBox">
                           <div class="SubSystemBox">
                              <div class="SubItemTp1Box">
                                 <a href="products.php" class="SubNvLinkBoxLnk">New Arrivals</a>
                                 <div class="SubItemBoxTp1">
                                    <div class="flex-wrap SubItemBoxTp1Items d-flex  align-items-center fullHeight justify-content-sm-center">
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dreamscape</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Exotic Nomad</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Imperial</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Mexicola</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dark & Moody</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Retro Remix</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink"> French Coast</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">White Bliss</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Luxe Style</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="SubSystemBox">
                              <div class="SubItemTp1Box">
                                 <a href="products.php" class="SubNvLinkBoxLnk">Seating</a>
                                 <div class="SubItemBoxTp1">
                                    <div class="flex-wrap SubItemBoxTp1Items d-flex  align-items-center fullHeight justify-content-sm-center">
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dreamscape</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Exotic Nomad</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Imperial</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Mexicola</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dark & Moody</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Retro Remix</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink"> French Coast</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">White Bliss</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Luxe Style</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="SubSystemBox">
                              <div class="SubItemTp1Box">
                                 <a href="products.php" class="SubNvLinkBoxLnk">Decor</a>
                                 <div class="SubItemBoxTp1">
                                    <div class="flex-wrap SubItemBoxTp1Items d-flex  align-items-center fullHeight justify-content-sm-center">
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dreamscape</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Exotic Nomad</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Imperial</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Mexicola</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dark & Moody</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Retro Remix</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink"> French Coast</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">White Bliss</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Luxe Style</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="SubSystemBox">
                              <div class="SubItemTp1Box">
                                 <a href="products.php" class="SubNvLinkBoxLnk">Tables</a>

                              </div>
                           </div>
                           <div class="SubSystemBox">
                              <div class="SubItemTp1Box">
                                 <a href="products.php" class="SubNvLinkBoxLnk">Bars & Food Stations</a>
                                 <div class="SubItemBoxTp1">
                                    <div class="flex-wrap SubItemBoxTp1Items d-flex  align-items-center fullHeight justify-content-sm-center">
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dreamscape</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Exotic Nomad</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Imperial</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Mexicola</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dark & Moody</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Retro Remix</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink"> French Coast</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">White Bliss</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Luxe Style</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="SubSystemBox">
                              <div class="SubItemTp1Box">
                                 <a href="products.php" class="SubNvLinkBoxLnk">Decor</a>
                                 <div class="SubItemBoxTp1">
                                    <div class="flex-wrap SubItemBoxTp1Items d-flex  align-items-center fullHeight justify-content-sm-center">
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dreamscape</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Exotic Nomad</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Imperial</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Mexicola</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dark & Moody</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Retro Remix</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink"> French Coast</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">White Bliss</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Luxe Style</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="NvLinkTp1 d-flex  align-items-center fullHeight">
                        <a href="gallery.php" class=" d-flex  align-items-center fullHeight justify-content-sm-center NvLinkTp1Item">Gallery</a>

                     </div>
                  </div>
               </div>
            </div>
            <div class="BxTpRight fullHeight align-items-center d-flex">
               <div class="tphoen DeskTopOption">800 MINT (6468)</div>
               <div class="PhoneIconBox"><img src="{{ bagisto_asset('themes/mint/assets/images/phone-icon.png') }}" alt="" /></div>
               <div class="cartIconBox"><a href="cart.php"><img src="{{ bagisto_asset('themes/mint/assets/images/truck-icon.png') }}" alt="" /></a></div>
            </div>

         </div>
      </div>
      <div class="MobileHeader">
         <div>
            <div class="MobileHeaderContent">
               <div>
                  <div class="d-flex">
                     <div class="MobleMenuIcnTp1 MobleSideMenu">
                        <img class="mobileMenuIconTp1" src="{{ bagisto_asset('themes/mint/assets/images/menu-icon.png') }}" alt="Side Menu" />
                     </div>
                     <div class="MobileSideNav">
                        <div class="FixedBoxTp1Wrp">
                           <div class="FixedBoxTp1">
                              <div class="FixedBoxTp1Header">
                                 <div class="row align-items-center ">
                                    <div class="col">
                                       <div>
                                          <div class="FixedBoxTp1Txt1">Mint</div>
                                          <div class="FixedBoxTp1Txt2">Event Rentals</div>
                                       </div>
                                    </div>
                                    <div class="col-auto"><span class="CloseBtn CloseSideNav">x</span></div>
                                 </div>
                              </div>
                              <div class="FixedBoxTp1Content">
                                 <div class="ItemBoxTp1">
                                    <div class="ItemBoxTp1Hd1">All Categories</div>
                                    <div class="ItemBoxTp1Content">
                                       <div class="ItemBoxTp1LinkBox">
                                          <a href="products.php" class="ItemBoxTp1Lnk">Catalogues <span class="SubChildActived"><i class="fa fa-angle-down" aria-hidden="true"></i></span></a>
                                          <a href="products.php" class="ItemBoxTp1Lnk">Themes<span class="SubChildActived"><i class="fa fa-angle-down" aria-hidden="true"></i></span></a>
                                          <a href="products.php" class="ItemBoxTp1Lnk">Collection<span class="SubChildActived"><i class="fa fa-angle-down" aria-hidden="true"></i></span></a>
                                          <a href="gallery.php" class="ItemBoxTp1Lnk">Gallery<span class="SubChildActived"><i class="fa fa-angle-down" aria-hidden="true"></i></span></a>
                                       </div>

                                    </div>
                                 </div>
                                 <div class="ItemBoxTp1">
                                    <div class="ItemBoxTp1Hd1">Who we are ? </div>
                                    <div class="ItemBoxTp1Content">
                                       <div class="ItemBoxTp1LinkBox">
                                          <a href="about-us.php" class="ItemBoxTp1Lnk1">About Us</a>
                                          <a href="download-catalog.php" class="ItemBoxTp1Lnk1">Downloads</a>
                                          <a href="cart.php" class="ItemBoxTp1Lnk1">Cart</a>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="ItemBoxTp1">
                                    <div class="ItemBoxTp1Hd1">Quick Support </div>
                                    <div class="ItemBoxTp1Content">
                                       <div class="ItemBoxTp1LinkBox">
                                          <a href="faq.php" class="ItemBoxTp1Lnk2">FAQs</a>
                                          <a href="#" class="ItemBoxTp1Lnk2">Press release</a>
                                          <a href="#" class="ItemBoxTp1Lnk2">Blog</a>
                                          <a href="terms-and-conditions.php" class="ItemBoxTp1Lnk2">Terms & Conditions</a>
                                          <a href="contact-us.php" class="ItemBoxTp1Lnk2">Contact Us</a>
                                          <a href="#" class="ItemBoxTp1Lnk2">Testimonials</a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="FixedBoxTp1Footer">
                                 <div class="social-area02">
                                    <a href="#" target="_blank"><i class="fa fb fa-facebook"></i></a>
                                    <a href="#" target="_blank"><i class="fa twt fa-twitter"></i></a>
                                    <a href="#" target="_blank"><i class="fa ig fa-instagram" aria-hidden="true"></i>
                                    </a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                  </div>
               </div>
               <div>
                  <a href="index.php" class="MobileHeroLogo">
                     <img src="{{ bagisto_asset('themes/mint/assets/images/logo.png') }}" alt="Logo" />
                  </a>
               </div>
               <div>
                  <div class="d-flex justify-content-end">
                     <div class="MobleMenuIcnTp1 MobileSearchMenu MarginAdjustmentTp1">
                        <img class="mobileMenuIconTp1" src="{{ bagisto_asset('themes/mint/assets/images/search-icon.png') }}" alt="" class="TopBarIconTp1 mrgRight50" />
                     </div>
                     <div class="MobileSearchBox">
                        <div class="FixedBoxTp1Wrp">
                           <div class="FixedBoxTp1">
                              <div class="FixedBoxTp1Header">
                                 <div class="row align-items-center ">
                                    <div class="col">
                                       <div>
                                          <div class="FixedBoxTp1Txt1">Mint</div>
                                          <div class="FixedBoxTp1Txt2">Event Rentals</div>
                                       </div>
                                    </div>
                                    <div class="col-auto"><span class="CloseBtn CloseSearch">x</span></div>
                                 </div>
                              </div>
                              <div class="FixedBoxTp1Content">
                                 <div class="ItemBoxTp1">
                                    <div class="ItemBoxTp1Hd1">Search Our Products</div>
                                    <div class="ItemBoxTp1Content">
                                       <div class="BoxTpSearcBox">
                                          <div>
                                             <form action="" class="MobileSearchBoxField">
                                                <div>
                                                   <div>
                                                      <input type="text" placeholder="Search Our Products here..">
                                                   </div>
                                                   <div>
                                                      <button class="SearchSubmit">Search</button>
                                                   </div>
                                                </div>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="FixedBoxTp1Footer">
                                 <div class="social-area02">
                                    <a href="#" target="_blank"><i class="fa fb fa-facebook"></i></a>
                                    <a href="#" target="_blank"><i class="fa twt fa-twitter"></i></a>
                                    <a href="#" target="_blank"><i class="fa ig fa-instagram" aria-hidden="true"></i>
                                    </a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="MobleMenuIcnTp1 "><a href="cart.php"><img class="mobileMenuIconTp2" src="{{ bagisto_asset('themes/mint/assets/images/truck-icon.png') }}" alt="" /></a></div>
                  </div>
               </div>
            </div>
         </div>
      </div>


   </header>


   <!-- header ends  -->

   <div class="slider-area">
      <div class="slider-active owl-dot-style owl-carousel">
         <div class="single-slider bg-img d-flex align-items-center justify-content-center" data-src="" style="background-image:url({{ bagisto_asset('themes/mint/assets/images/header-img01.jpg') }});">
            <div class="slider-content pt-100" data-aos="fade-down" data-aos-duration="500">
               <div class="slider-content-wrap slider-animated-1">
                  <h2 class="animated">#minteventrental</h2>
               </div>
            </div>
         </div>
         <div class="single-slider bg-img d-flex align-items-center justify-content-center" data-src="" style="background-image:url({{ bagisto_asset('themes/mint/assets/images/header-img02.jpg') }});">
            <div class="slider-content pt-100">
               <div class="slider-content-wrap slider-animated-1">
                  <h2 class="animated">#minteventrental</h2>
               </div>
            </div>
         </div>

      </div>
   </div>



   


   <div class="SiteContentBox">

<div class="BoxTpArrival position-relative new-arrival-area"  data-aos="fade-up" data-aos-duration="1100">
   <div class="container">
      <div class="MobOnly SwipingBoxTp1"  data-sticky_column>
         <div class="HorizontalSwiperBox">
            <div class="HorizontalSwiperBoxContainer">
               <div class="BoxContainer">
                  <a href="products.php" class="MobLinkTp1">
                     <div class="grid-icon grid-icon--line">
                        <span class="layer layer--primary">
                           <span></span><span></span><span></span>
                        </span>
                        <span class="layer layer--secondary">
                           <span></span><span></span><span></span>
                        </span>
                        <span class="layer layer--tertiary">
                           <span></span><span></span><span></span>
                        </span>
                     </div>
                     <div class="text-center TxtTpMob1">
                        Catalogue
                     </div>
                  </a>
               </div>
               <div class="BoxContainer">
                  <a href="products.php" class="MobLinkTp1">
                  <div class="grid-icon grid-icon--line">
                        <span class="layer layer--primary">
                           <span></span><span></span><span></span>
                        </span>
                        <span class="layer layer--secondary">
                           <span></span><span></span><span></span>
                        </span>
                        <span class="layer layer--tertiary">
                           <span></span><span></span><span></span>
                        </span>
                     </div>
                     <div class="text-center TxtTpMob1">
                        Themes
                     </div>
                  </a>
               </div>
               <div class="BoxContainer">
                  <a href="products.php" class="MobLinkTp1">
                  <div class="grid-icon grid-icon--line">
                        <span class="layer layer--primary">
                           <span></span><span></span><span></span>
                        </span>
                        <span class="layer layer--secondary">
                           <span></span><span></span><span></span>
                        </span>
                        <span class="layer layer--tertiary">
                           <span></span><span></span><span></span>
                        </span>
                     </div>
                     <div class="text-center TxtTpMob1">
                        Collections
                     </div>
                  </a>
               </div>
               <div class="BoxContainer">
                  <a href="products.php" class="MobLinkTp1">
                  <div class="grid-icon grid-icon--line">
                        <span class="layer layer--primary">
                           <span></span><span></span><span></span>
                        </span>
                        <span class="layer layer--secondary">
                           <span></span><span></span><span></span>
                        </span>
                        <span class="layer layer--tertiary">
                           <span></span><span></span><span></span>
                        </span>
                     </div>
                     <div class="text-center TxtTpMob1">
                       Gallery
                     </div>
                  </a>
               </div>
               <div class="BoxContainer">
                  <a href="products.php" class="MobLinkTp1">
                  <div class="grid-icon grid-icon--line">
                        <span class="layer layer--primary">
                           <span></span><span></span><span></span>
                        </span>
                        <span class="layer layer--secondary">
                           <span></span><span></span><span></span>
                        </span>
                        <span class="layer layer--tertiary">
                           <span></span><span></span><span></span>
                        </span>
                     </div>
                     <div class="text-center TxtTpMob1">
                        Downloads
                     </div>
                  </a>
               </div>
               <div class="BoxContainer">
                  <a href="products.php" class="MobLinkTp1">
                  <div class="grid-icon grid-icon--line">
                        <span class="layer layer--primary">
                           <span></span><span></span><span></span>
                        </span>
                        <span class="layer layer--secondary">
                           <span></span><span></span><span></span>
                        </span>
                        <span class="layer layer--tertiary">
                           <span></span><span></span><span></span>
                        </span>
                     </div>
                     <div class="text-center TxtTpMob1">
                        Cart
                     </div>
                  </a>
               </div>
            </div>
         </div>
      </div>
      <div class="MobOnly MobileTxtTp1 clearfix">New Arrivals <a href="#">Explore</a></div>
      <div class="MobileArrangeBxTp1">
         <div class="MobOnly HomeSubSliderTp1 gallery-area ContentBlockTp1">
            <div class="container-fluid">
               <section class="row bborder HorizontalSliderActive" data-aos="fade-up" data-aos-duration="1100">
                  <aside class="col-6 col-sm-4 col-md-2 col-lg-2 col-xl-2">
                     <div class="featureebox">
                        <div class="FeaturedHoverBox" data-toggle="modal" data-target="#ProductDetailModal">
                           <figure><img class="lazy" src="{{ bagisto_asset('themes/mint/assets/images/ajaxloader.png') }}" data-src="{{ bagisto_asset('themes/mint/assets/images/chair01.jpg') }}" alt="Chair" /></figure>
                           <h2>RENEE DINING CHAIR</h2>
                           <div class="colorbox">
                              
                              Various Colors
                           </div>
                           <div class="colordots">
                              <ul>
                                 <li><img src="{{ bagisto_asset('themes/mint/assets/images/dot01.jpg') }}" alt="" /></li>
                                 <li><img src="{{ bagisto_asset('themes/mint/assets/images/dot02.jpg') }}" alt="" /></li>
                                 <li><img src="{{ bagisto_asset('themes/mint/assets/images/dot03.jpg') }}" alt="" /></li>
                              </ul>

                           </div>
                           <div class="selectcolor">(Select A Color)</div>

                           <!--<a href="product-details.php" class="addtocart-btn">
                              <div class="inner"></div>
                           </a>-->
                        </div>
                     </div>
                  </aside>
                  <aside class="col-6 col-sm-4 col-md-2 col-lg-2 col-xl-2">
                     <div class="featureebox">
                        <div class="FeaturedHoverBox" data-toggle="modal" data-target="#ProductDetailModal">
                           <figure><img class="lazy" src="{{ bagisto_asset('themes/mint/assets/images/ajaxloader.png') }}" data-src="{{ bagisto_asset('themes/mint/assets/images/chair02.jpg') }}" alt="Chair" /></figure>
                           <h2>RENEE DINING CHAIR</h2>
                           <div class="colorbox">
                              
                              Various Colors

                           </div>
                           <div class="colordots">
                              <ul>
                                 <li><img src="{{ bagisto_asset('themes/mint/assets/images/dot01.jpg') }}" alt="" /></li>
                                 <li><img src="{{ bagisto_asset('themes/mint/assets/images/dot02.jpg') }}" alt="" /></li>
                                 <li><img src="{{ bagisto_asset('themes/mint/assets/images/dot03.jpg') }}" alt="" /></li>

                              </ul>

                           </div>
                           <div class="selectcolor">(Select A Color)</div>

                           <!--<a href="product-details.php" class="addtocart-btn">
                              <div class="inner"></div>
                           </a>-->
                        </div>
                     </div>
                  </aside>
                  <aside class="col-6 col-sm-4 col-md-2 col-lg-2 col-xl-2">
                     <div class="featureebox">
                        <div class="FeaturedHoverBox" data-toggle="modal" data-target="#ProductDetailModal">
                           <figure><img class="lazy" src="{{ bagisto_asset('themes/mint/assets/images/ajaxloader.png') }}" data-src="{{ bagisto_asset('themes/mint/assets/images/chair01.jpg') }}" alt="Chair" /></figure>
                           <h2>RENEE DINING CHAIR</h2>
                           <div class="colorbox">
                              
                              Various Colors

                           </div>
                           <div class="colordots">
                              <ul>
                                 <li><img src="{{ bagisto_asset('themes/mint/assets/images/dot01.jpg') }}" alt="" /></li>
                                 <li><img src="{{ bagisto_asset('themes/mint/assets/images/dot02.jpg') }}" alt="" /></li>
                                 <li><img src="{{ bagisto_asset('themes/mint/assets/images/dot03.jpg') }}" alt="" /></li>

                              </ul>

                           </div>
                           <div class="selectcolor">(Select A Color)</div>

                           <!--<a href="product-details.php" class="addtocart-btn">
                              <div class="inner"></div>
                           </a>-->
                        </div>

                     </div>
                  </aside>
                  <aside class="col-6 col-sm-4 col-md-2 col-lg-2 col-xl-2">
                     <div class="featureebox">
                        <div class="FeaturedHoverBox" data-toggle="modal" data-target="#ProductDetailModal">
                           <figure><img class="lazy" src="{{ bagisto_asset('themes/mint/assets/images/ajaxloader.png') }}" data-src="{{ bagisto_asset('themes/mint/assets/images/chair02.jpg') }}" alt="Chair" /></figure>
                           <h2>RENEE DINING CHAIR</h2>
                           <div class="colorbox">
                              
                              Various Colors

                           </div>
                           <div class="colordots">
                              <ul>
                                 <li><img src="{{ bagisto_asset('themes/mint/assets/images/dot01.jpg') }}" alt="" /></li>
                                 <li><img src="{{ bagisto_asset('themes/mint/assets/images/dot02.jpg') }}" alt="" /></li>
                                 <li><img src="{{ bagisto_asset('themes/mint/assets/images/dot03.jpg') }}" alt="" /></li>

                              </ul>

                           </div>
                           <div class="selectcolor">(Select A Color)</div>

                           <!--<a href="product-details.php" class="addtocart-btn">
                              <div class="inner"></div>
                           </a>-->
                        </div>
                     </div>
                  </aside>
                  <aside class="col-6 col-sm-4 col-md-2 col-lg-2 col-xl-2">
                     <div class="featureebox">
                        <div class="FeaturedHoverBox" data-toggle="modal" data-target="#ProductDetailModal">
                           <figure><img class="lazy" src="{{ bagisto_asset('themes/mint/assets/images/ajaxloader.png') }}" data-src="{{ bagisto_asset('themes/mint/assets/images/chair01.jpg') }}" alt="Chair" /></figure>
                           <h2>RENEE DINING CHAIR</h2>
                           <div class="colorbox">
                              
                              Various Colors
                           </div>
                           <div class="colordots">
                              <ul>
                                 <li><img src="{{ bagisto_asset('themes/mint/assets/images/dot01.jpg') }}" alt="" /></li>
                                 <li><img src="{{ bagisto_asset('themes/mint/assets/images/dot02.jpg') }}" alt="" /></li>
                                 <li><img src="{{ bagisto_asset('themes/mint/assets/images/dot03.jpg') }}" alt="" /></li>

                              </ul>

                           </div>
                           <div class="selectcolor">(Select A Color)</div>

                           <!--<a href="product-details.php" class="addtocart-btn">
                              <div class="inner"></div>
                           </a>-->
                        </div>
                     </div>
                  </aside>
                  <aside class="col-6 col-sm-4 col-md-2 col-lg-2 col-xl-2">
                     <div class="featureebox">
                        <div class="FeaturedHoverBox" data-toggle="modal" data-target="#ProductDetailModal">
                           <figure><img class="lazy" src="{{ bagisto_asset('themes/mint/assets/images/ajaxloader.png') }}" data-src="{{ bagisto_asset('themes/mint/assets/images/chair02.jpg') }}" alt="Chair" /></figure>
                           <h2>RENEE DINING CHAIR</h2>
                           <div class="colorbox">
                              
                              Various Colors

                           </div>
                           <div class="colordots">
                              <ul>
                                 <li><img src="{{ bagisto_asset('themes/mint/assets/images/dot01.jpg') }}" alt="" /></li>
                                 <li><img src="{{ bagisto_asset('themes/mint/assets/images/dot02.jpg') }}" alt="" /></li>
                                 <li><img src="{{ bagisto_asset('themes/mint/assets/images/dot03.jpg') }}" alt="" /></li>

                              </ul>

                           </div>
                           <div class="selectcolor">(Select A Color)</div>

                           <!--<a href="product-details.php" class="addtocart-btn">
                              <div class="inner"></div>
                           </a>-->
                        </div>
                     </div>
                  </aside>


               </section>
            </div>
         </div>
         <section class="row align-items-center mobileBgWhite DesktopOnly">
            <aside class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8">
               <div class="ArrowMiddle">
               <div class="owl-carousel owl-theme newArrivalSlider">
                  <div class="item">
                     <div class="slidebox">
                        <figure><img src="{{ bagisto_asset('themes/mint/assets/images/arrival01.jpg') }}" alt="My Logo"></figure>
                        <h4>Scandinavian<br>
                           Dining chair
                        </h4>
                        <p>2 Colors</p>
                        <div class="BottomLinkBox">
                           <a href="productdetails.php" class="addtocart-btn-tp-1">
                              <img src="{{ bagisto_asset('themes/mint/assets/images/addtocartbtn02.png') }}" alt="">
                           </a>
                        </div>
                     </div>
                  </div>
                  <div class="item">
                     <div class="slidebox">
                     <figure><img src="{{ bagisto_asset('themes/mint/assets/images/arrival02.jpg') }}" alt="My Logo"></figure>
                        <h4>Scandinavian<br>
                           Dining chair
                        </h4>
                        <p>2 Colors</p>
                        <div class="BottomLinkBox">
                           <a href="productdetails.php" class="addtocart-btn-tp-1">
                              <img src="{{ bagisto_asset('themes/mint/assets/images/addtocartbtn02.png') }}" alt="">
                           </a>
                        </div>
                     </div>
                  </div>
                  <div class="item">
                     <div class="slidebox">
                     <figure><img src="{{ bagisto_asset('themes/mint/assets/images/arrival03.jpg') }}" alt="My Logo"></figure>
                        <h4>Scandinavian<br>
                           Dining chair
                        </h4>
                        <p>2 Colors</p>
                        <div class="BottomLinkBox">
                           <a href="productdetails.php" class="addtocart-btn-tp-1">
                              <img src="{{ bagisto_asset('themes/mint/assets/images/addtocartbtn02.png') }}" alt="">
                           </a>
                        </div>
                     </div>
                  </div>
                  <div class="item">
                     <div class="slidebox">
                        <figure><img src="{{ bagisto_asset('themes/mint/assets/images/arrival01.jpg') }}" alt="My Logo"></figure>
                        <h4>Scandinavian<br>
                           Dining chair
                        </h4>
                        <p>2 Colors</p>
                        <div class="BottomLinkBox">
                           <a href="productdetails.php" class="addtocart-btn-tp-1">
                              <img src="{{ bagisto_asset('themes/mint/assets/images/addtocartbtn02.png') }}" alt="">
                           </a>
                        </div>
                     </div>
                  </div>
                  <div class="item">
                     <div class="slidebox">
                     <figure><img src="{{ bagisto_asset('themes/mint/assets/images/arrival02.jpg') }}" alt="My Logo"></figure>
                        <h4>Scandinavian<br>
                           Dining chair
                        </h4>
                        <p>2 Colors</p>
                        <div class="BottomLinkBox">
                           <a href="productdetails.php" class="addtocart-btn-tp-1">
                              <img src="{{ bagisto_asset('themes/mint/assets/images/addtocartbtn02.png') }}" alt="">
                           </a>
                        </div>
                     </div>
                  </div>
                  <div class="item">
                     <div class="slidebox">
                     <figure><img src="{{ bagisto_asset('themes/mint/assets/images/arrival03.jpg') }}" alt="My Logo"></figure>
                        <h4>Scandinavian<br>
                           Dining chair
                        </h4>
                        <p>2 Colors</p>
                        <div class="BottomLinkBox">
                           <a href="productdetails.php" class="addtocart-btn-tp-1">
                              <img src="{{ bagisto_asset('themes/mint/assets/images/addtocartbtn02.png') }}" alt="">
                           </a>
                        </div>
                     </div>
                  </div>
               </div>
               </div> 
               
               
            </aside>
            <aside class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 BxTpItm1">
               <h3>New Arrivals</h3>
               <h2>NOVEMBER 2019</h2>
               <a href="products.php" class="exp-btn">
                  <div class="inner"></div>
               </a>
            </aside>
         </section>
      </div>
   </div>
</div>
<div class="about-area"  data-aos="fade-up" data-aos-duration="1100">
   <div class="caption-area">
      <h3>About Us</h3>
      <h2>We Know Rental</h2>
      <a href="aboutus.php" class="showmebtn">
         <div class="inner"></div>
      </a>
   </div>
</div>
<div class="downloads-area HomeDownloadArea" data-aos="fade-up" data-aos-duration="1100">
   <div class="catalouge">Catalogue</div>
   <div class="downloadsbg">Downloads</div>
   <div class="container">
      <section class="row">
         <aside class="col-12 col-sm-12 col-md-12 col-lg-7 col-xl-7">
            <h3>Downloads</h3>
            <h2>GET OUR Catalogue!</h2>
            <ul>
               <section class="row">
                  <aside class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                     <li>Master Catalog 2018/2019</li>
                     <li>Party & Wedding Catalog 2018/2019</li>
                     <li>Exhibition Catalog 2018/2019</li>
                  </aside>
                  <aside class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                     <li>Conference Catalog 2018/2019</li>
                     <li>Tablescapes Catalog 2018/2019</li>
                     <li>Prop Catalog 2018/2019</li>
                  </aside>
               </section>
            </ul>
            <a href="catalogdownload.php" class="fancybox fancybox.iframe dl-btn">
               <div class="inner"></div>
            </a>
         </aside>
         <aside class="col-12 col-sm-12 col-md-12 col-lg-5 col-xl-5">
            <section class="row">
               <aside class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                  <figure><img src="{{ bagisto_asset('themes/mint/assets/images/dl-img01.jpg') }}" alt="" /></figure>
               </aside>
               <aside class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                  <figure><img src="{{ bagisto_asset('themes/mint/assets/images/dl-img01.jpg') }}" alt="" /></figure>
               </aside>
            </section>
         </aside>
      </section>
   </div>
</div>
<div class="range-area" data-aos="fade-up" data-aos-duration="1100">
   <div class="caption-area">
      <h3>Lorem ipsum</h3>
      <h2>Morrocan Sunset</h2>
      <a href="products.php" class="vrangebtn">
         <div class="inner"></div>
      </a>
   </div>
</div>
<div class="deliverytracking-area HomeDeliveryTrackingArea"  data-aos="fade-up" data-aos-duration="1100">
   <div class="deliveryl">Delivery</div>
   <div class="tranckingc">Tracking</div>
   <div class="container">
      <section class="row">
         <aside class="col-12 col-sm-12 col-md-12 col-lg-7 col-xl-7">
            <h3>ON TIME, EVERY TIME</h3>
            <h2>DELIVERY TRACKING</h2>
            <ul>
               <li>View all your orders online</li>
               <li>Track your deliveries in real time</li>
               <li>Receive prompt alerts</li>
            </ul>
            <p>*for corporate customers only</p>
            <a href="<contactus.php" class="partner-btn">Partner with us</a>
         </aside>
         <aside class="col-12 col-sm-12 col-md-12 col-lg-5 col-xl-5">
            <figure><img src="{{ bagisto_asset('themes/mint/assets/images/mobile-phone.jpg') }}" alt="phone" /></figure>
         </aside>
      </section>
   </div>
</div>
<div class="bookameeting" data-aos="fade-up" data-aos-duration="1100">
   <div class="caption-area">
      <h3>SEEING IS BELIEVING</h3>
      <h2>SWING BY OUR HQ</h2>
      <a href="<contactus.php" class="bookameeting-btn">
         <div class="inner"></div>
      </a>
   </div>
</div>

</div>
   <!-- contends end here -->

   <footer class="footer-area" data-aos="fade-up" data-aos-duration="1100">
      <div class="container">
         <section class="row">
            <aside class="col-12 col-sm-6 col-md-4 col-lg col-xl">
               <h2 class="MobileLinkTp101">
                  RENTAL <br> FURNITURE
                  <i class="fa fa-plus PlusIcon1 IconTpMob1" aria-hidden="true"></i>
                  <i class="fa fa-minus MinusIcon1 IconTpMob1" aria-hidden="true"></i>
               </h2>
               <ul class="MobileBoxTp101">
                  <li><a href="products.php">Chairs</a></li>
                  <li><a href="products.php">Tables</a></li>
                  <li><a href="products.php">Bars</a></li>
                  <li><a href="products.php">Food Stations</a></li>
                  <li><a href="products.php">Decor</a></li>
                  <li><a href="products.php">Hospitality</a></li>
                  <li><a href="products.php">Collections</a></li>
                  <li><a href="products.php">Dancefloors</a></li>
                  <li><a href="products.php">Outdoor</a></li>
                  <li><a href="products.php">Themes</a></li>
               </ul>
            </aside>
            <aside class="col-12 col-sm-6 col-md-4 col-lg col-xl">
               <h2 class="MobileLinkTp101">TABLEWARE<br> RENTAL<i class="fa fa-plus PlusIcon1 IconTpMob1" aria-hidden="true"></i>
                  <i class="fa fa-minus MinusIcon1 IconTpMob1" aria-hidden="true"></i></h2>
               <ul class="MobileBoxTp101">
                  <li><a href="products.php">Charger Plates</a></li>
                  <li><a href="products.php">Crockery</a></li>
                  <li><a href="products.php">Cutlery</a></li>
                  <li><a href="products.php">Glasses</a></li>
                  <li><a href="products.php">Linen</a></li>
                  <li><a href="products.php">Equipment</a></li>
                  <li><a href="products.php">Bar Service</a></li>
               </ul>
            </aside>
            <aside class="col-12 col-sm-6 col-md-4 col-lg col-xl">
               <h2 class="MobileLinkTp101">GALLERIES<i class="fa fa-plus PlusIcon1 IconTpMob1" aria-hidden="true"></i>
                  <i class="fa fa-minus MinusIcon1 IconTpMob1" aria-hidden="true"></i></h2>
               <ul class="MobileBoxTp101">
                  <li><a href="products.php">Party</a></li>
                  <li><a href="products.php">Wedding</a></li>
                  <li><a href="products.php">Conference</a></li>
                  <li><a href="products.php">Coporate Events</a></li>
                  <li><a href="products.php">Major Events</a></li>
                  <li><a href="products.php">Exhibitions</a></li>
               </ul>
            </aside>
            <aside class="col-12 col-sm-6 col-md-4 col-lg col-xl">
               <h2 class="MobileLinkTp101">GENERAL<i class="fa fa-plus PlusIcon1 IconTpMob1" aria-hidden="true"></i>
                  <i class="fa fa-minus MinusIcon1 IconTpMob1" aria-hidden="true"></i></h2>
               <ul class="MobileBoxTp101">
                  <li><a href="faq.php">FAQ’s</a></li>
                  <li><a href="termsandconditions.php">Rental Terms & Contitions</a></li>
                  <li><a href="products.php">Conference</a></li>
                  <li><a href="products.php">Coporate Events</a></li>
                  <li><a href="products.php">Major Events</a></li>
                  <li><a href="products.php">Exhibitions</a></li>
               </ul>
            </aside>
            <aside class="col-12 col-sm-6 col-md-4 col-lg col-xl">
               <h2 class="MobileLinkTp101">CONTACT US<i class="fa fa-plus PlusIcon1 IconTpMob1" aria-hidden="true"></i>
                  <i class="fa fa-minus MinusIcon1 IconTpMob1" aria-hidden="true"></i></h2>
               <div class="MobileBoxTp101">
                  <p> Within UAE: 800 6468 (MINT)
                     Outside UAE: +971 4 3474340
                  </p>
                  <p>Whatsapp: +971 55 2149122</p>
                  <p>Email: <br>
                     <a href="mailto:sales@minteventrentals.com">sales@minteventrentals.com</a>
                  </p>
                  <p>MINT HQ:<br>
                     <a href="#">www.googlemaps.com</a>
                  </p>
                  <p>MINT DIC WH:<br>
                     <a href="#">www.googlemaps.com</a>
                  </p>
                  <p>MINT AUH WH:<br>
                     <a href="#">www.googlemaps.com</a>
                  </p>
               </div>
            </aside>
         </section>
         <div class="social-area">


            <h3>Connect With Us</h3>
            <a href="#" target="_blank"><i class="fa fb fa-facebook"></i></a>
            <a href="#" target="_blank"><i class="fa twt fa-twitter"></i></a>

            <a href="#" target="_blank"><i class="fa ig fa-instagram" aria-hidden="true"></i>
            </a>



            <div class="clear"></div>
         </div>
         <div class="clear"></div>
      </div>
      <div class="clear"></div>
   </footer>
   <footer class="footer-bottom">
      <div class="container">
         © Copyright 2019. Mint Event Rentals. All Rights Reserved. | <a href="#">Privay Policy</a> | <a href="termsandconditions.php">Terms & Conditions</a> | <a href="faq.php">FAQ</a>
      </div>
   </footer>
   <!-- all js here -->
   <script src="{{ bagisto_asset('themes/mint/assets/js/vendor/jquery-1.12.0.min.js') }}"></script>
   <script src="{{ bagisto_asset('themes/mint/assets/js/jquery.lazy.min.js') }}"></script>
   <script src="{{ bagisto_asset('themes/mint/assets/js/popper.js') }}"></script>
   <script src="{{ bagisto_asset('themes/mint/assets/js/bootstrap.min.js') }}"></script>
   <script src="{{ bagisto_asset('themes/mint/assets/js/ajax-mail.js') }}"></script>
   <script src="{{ bagisto_asset('themes/mint/assets/js/plugins.js') }}"></script>
   <script src="{{ bagisto_asset('themes/mint/assets/js/moment.js') }}"></script>
   <script src="{{ bagisto_asset('themes/mint/assets/js/jquery.daterangepicker.min.js') }}"></script>
   <script src="{{ bagisto_asset('themes/mint/assets/js/stickyLatest.js') }}"></script>
   <script type="text/javascript">
      $(window).scroll(function() {
         var sticky = $('.HeroLogo'),
            scroll = $(window).scrollTop();

         if (scroll >= 100) sticky.addClass('fixedHeight');
         else sticky.removeClass('fixedHeight');
      });
      jQuery(document).ready(function($) {
         $(".SiteContentBox").css('display', 'none');
         $('html,body').animate({
            scrollTop: 0
         }, '20');
         $('.slider-active').on('changed.owl.carousel', function(event) {
            $(".SiteContentBox").css('display', 'block');
         });
         $('.slider-active').owlCarousel({
            callbacks: true,
            loop: true,
            nav: true,
            autoplay: false,
            autoplayTimeout: 5000,
            animateOut: 'fadeOut',
            animateIn: 'fadeIn',
            //navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
            navText: ['<div class="smouseicon"><img src="{{ bagisto_asset('themes/mint/assets/images/mouse-icon.png') }}" alt=""/></div>'],
            item: 1,
            //navElement:'smouseicon',
            responsive: {
               0: {
                  items: 1,
                  autoplay: true,
                  autoplayTimeout: 5000,
               },
               768: {
                  items: 1
               },
               1000: {
                  items: 1
               }
            }
         })
      });
      var didScroll;
      var lastScrollTop = 0;
      var delta = 5;
      var navbarHeight = $('.BXtpHeaderTp1').outerHeight();

      $(window).scroll(function(event) {
         didScroll = true;
      });

      setInterval(function() {
         if (didScroll) {
            hasScrolled();
            didScroll = false;
         }
      }, 250);

      function hasScrolled() {
         var st = $(this).scrollTop();
         if (Math.abs(lastScrollTop - st) <= delta)
            return;
         if (st > lastScrollTop && st > navbarHeight) {
            $('.BXtpHeaderTp1').removeClass('nav-down').addClass('nav-up');
            $('body').removeClass('nav-down-active').addClass('nav-up-active');
         } else {
            if (st + $(window).height() < $(document).height()) {
               $('.BXtpHeaderTp1').removeClass('nav-up').addClass('nav-down');
               $('body').removeClass('nav-up-active').addClass('nav-down-active');
            }
         }

         lastScrollTop = st;
      }



      $(function() {

         $('.lazy').Lazy({
            effect: 'fadeIn',
            effectTime: 100,
            visibleOnly: true
         });
      });
      $(document).ready(function() {
         $('#reservation').daterangepicker(null, function(start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
         });

         var optionSet2 = {
            startDate: moment().subtract('days', 7),
            endDate: moment(),
            opens: 'bottom',
            ranges: {
               'Today': [moment(), moment()],
               'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
               'Last 7 Days': [moment().subtract('days', 6), moment()],
               'Last 30 Days': [moment().subtract('days', 29), moment()],
               'This Month': [moment().startOf('month'), moment().endOf('month')],
               'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
            }
         };
         $('#reservation1').daterangepicker(null, function(start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
         });
         $('#PickDateBox').click(function() {
            $('#reservation1').click();
            $('#reservation1').data('daterangepicker').setOptions(optionSet2, cb);
         });
      });
   </script>

   <script src="{{ bagisto_asset('themes/mint/assets/js/main.js') }}"></script>

</body>

</html>